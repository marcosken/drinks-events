import { Switch, Route } from "react-router-dom";
import Home from "../pages/Home";
import GetTogether from "../pages/GetTogether";
import Graduation from "../pages/Graduation";
import Wedding from "../pages/Wedding";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/getTogether" component={GetTogether} />
      <Route path="/graduation" component={Graduation} />
      <Route path="/wedding" component={Wedding} />
    </Switch>
  );
};

export default Routes;

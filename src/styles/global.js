import { createGlobalStyle } from "styled-components";
import wood from "../assets/wood.png";

const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        list-style: none;
    }

    :root {
        --black: #000401;
        --brown: #4d0900;
        --white: #e5e8e8;
        --gray: #30333c;
        --lightGray: #5c646d;
        --font-title: 'Fredericka the Great', cursive;
        --font-text: 'Special Elite', cursive;
    }

    body {
        background-image: url(${wood});
        font-family: var(--font-text)
    }

    h1, h2, h3 {
        font-family: var(--font-title)
    }

    button {
        border: none;
    }
`;

export default GlobalStyle;

import EventListCard from "../../components/EventListCard";
import { useGetTogetherDrinks } from "../../providers/getTogetherDrinks";
import { Container, EventList } from "./styles";

const GetTogether = () => {
  const { getTogetherDrinks } = useGetTogetherDrinks();

  return (
    <Container>
      <h1>Get-together</h1>
      {getTogetherDrinks.length === 0 && <h2>EMPTY LIST</h2>}
      <EventList>
        {getTogetherDrinks.map((drink, index) => (
          <EventListCard
            name="getTogether"
            key={index}
            drink={drink}
            index={index}
          />
        ))}
      </EventList>
    </Container>
  );
};
export default GetTogether;

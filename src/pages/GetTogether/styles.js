import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 48px;

  h1 {
    margin: 15px 0;
    color: var(--white);
  }

  h2 {
    color: var(--white);
    position: absolute;
    top: 50%;
  }
`;

export const EventList = styled.ul`
  width: 100%;
  margin-top: 45px;

  @media (min-width: 768px) {
    width: 60%;
  }
`;

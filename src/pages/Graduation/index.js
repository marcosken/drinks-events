import EventListCard from "../../components/EventListCard";
import { useGraduationDrinks } from "../../providers/graduationDrinks";
import { Container, EventList } from "./styles";

const Graduation = () => {
  const { graduationDrinks } = useGraduationDrinks();

  return (
    <Container>
      <h1>Graduation Party</h1>
      {graduationDrinks.length === 0 && <h2>EMPTY LIST</h2>}
      <EventList>
        {graduationDrinks.map((drink, index) => (
          <EventListCard
            name="graduation"
            key={index}
            drink={drink}
            index={index}
          />
        ))}
      </EventList>
    </Container>
  );
};
export default Graduation;

import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 48px;

  h1 {
    margin: 15px 0;
    color: var(--white);
  }
`;

export const Content = styled.div`
  display: flex;
  justify-content: center;
`;

export const DrinkList = styled.ul`
  margin-top: 45px;
  row-gap: 15px;
  column-gap: 20px;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  list-style-type: none;
  @media (max-width: 945px) {
    grid-template-columns: repeat(2, 1fr);
  }
  @media (max-width: 590px) {
    grid-template-columns: repeat(1, 1fr);
  }
`;

export const Buttons = styled.div`
  margin: 25px 0;
  button {
    cursor: pointer;
    padding: 15px;
    font-size: 0.9rem;
    font-weight: bold;
    background-color: var(--brown);
    color: var(--white);
    border-radius: 4px;
    border: none;
    margin: 0 40px;

    &:hover {
      background-color: var(--black);
    }
  }
`;

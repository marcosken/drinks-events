import { useEffect, useState } from "react";
import CatalogueCard from "../../components/CatalogueCard";
import ChoiceOfEvent from "../../components/ChoiceOfEvent";
import { useCatalogue } from "../../providers/catalogue";
import { Container, Content, DrinkList } from "./styles";
import loading from "../../assets/loading.svg";

const Home = () => {
  const { catalogue, nextPage, isLoading } = useCatalogue();
  const [isShow, setIsShow] = useState(false);
  const [selectedDrink, setSelectedDrink] = useState({});

  useEffect(() => {
    const intersectionObserver = new IntersectionObserver((entries) => {
      if (entries.some((entry) => entry.isIntersecting)) {
        nextPage();
      }
    });

    intersectionObserver.observe(document.querySelector("#sentinela"));

    return () => intersectionObserver.disconnect();
  }, []);

  const handleClick = (drink) => {
    setSelectedDrink(drink);
    setIsShow(true);
  };

  return (
    <Container>
      {isShow && (
        <ChoiceOfEvent selectedDrink={selectedDrink} setIsShow={setIsShow} />
      )}
      <h1>Drinks Catalogue</h1>
      <Content>
        <DrinkList>
          {catalogue &&
            catalogue.map((drink, index) => (
              <CatalogueCard
                key={index}
                drink={drink}
                handleClick={handleClick}
              />
            ))}
        </DrinkList>
      </Content>
      <div id="sentinela" style={{ marginTop: "50px" }}>
        {isLoading && <img src={loading} alt="loading" />}
      </div>
    </Container>
  );
};

export default Home;

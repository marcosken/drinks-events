import EventListCard from "../../components/EventListCard";
import { useWeddingDrinks } from "../../providers/weddingDrinks";
import { Container, EventList } from "./styles";

const Wedding = () => {
  const { weddingDrinks } = useWeddingDrinks();

  return (
    <Container>
      <h1>Wedding Party</h1>
      {weddingDrinks.length === 0 && <h2>EMPTY LIST</h2>}
      <EventList>
        {weddingDrinks.map((drink, index) => (
          <EventListCard
            name="wedding"
            key={index}
            drink={drink}
            index={index}
          />
        ))}
      </EventList>
    </Container>
  );
};
export default Wedding;

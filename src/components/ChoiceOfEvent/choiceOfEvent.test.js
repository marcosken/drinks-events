import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ChoiceOfEvent from "./";
import { toast } from "react-toastify";

jest.mock("../../providers/graduationDrinks", () => {
  return {
    useGraduationDrinks: () => {
      return {
        graduationDrinks: [],
        addToGraduationDrinks: jest.fn(),
      };
    },
  };
});

jest.mock("../../providers/getTogetherDrinks", () => {
  return {
    useGetTogetherDrinks: () => {
      return {
        getTogetherDrinks: [],
        addToGetTogetherDrinks: jest.fn(),
      };
    },
  };
});

jest.mock("../../providers/weddingDrinks", () => {
  return {
    useWeddingDrinks: () => {
      return {
        weddingDrinks: [],
        addToWeddingDrinks: jest.fn(),
      };
    },
  };
});

jest.mock("react-toastify");
const mockedToast = toast;

describe("When everything is ok", () => {
  test("Should call toast.success when the user clicks on the Graduation Party button for the first time", () => {
    render(<ChoiceOfEvent selectedDrink={{ id: 1, name: "Drink Foo" }} />);

    const graduationPartyButton = screen.getByText("Graduation Party");
    userEvent.click(graduationPartyButton);

    expect(mockedToast.success).toHaveBeenCalled();
  });
});

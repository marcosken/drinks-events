import { Container, Content, Buttons } from "./styles";
import { useGetTogetherDrinks } from "../../providers/getTogetherDrinks";
import { useGraduationDrinks } from "../../providers/graduationDrinks";
import { useWeddingDrinks } from "../../providers/weddingDrinks";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import { toast } from "react-toastify";

const useStyle = makeStyles({
  closeButton: {
    height: "50px",
  },
});

const ChoiceOfEvent = ({ selectedDrink, setIsShow }) => {
  const { graduationDrinks, addToGraduationDrinks } = useGraduationDrinks();
  const { getTogetherDrinks, addToGetTogetherDrinks } = useGetTogetherDrinks();
  const { weddingDrinks, addToWeddingDrinks } = useWeddingDrinks();

  const handleClickGraduation = (selectedDrink) => {
    addToGraduationDrinks(selectedDrink);
    const isRepeated = graduationDrinks.some(
      (item) => item.id === selectedDrink.id
    );

    if (isRepeated) {
      toast.error(
        `${selectedDrink.name} is already on the graduation party list! Add it to another event or select another drink.`
      );
    } else {
      toast.success(
        `${selectedDrink.name} has been added to the graduation party list.`
      );
    }
  };

  const handleClickGetTogether = (selectedDrink) => {
    addToGetTogetherDrinks(selectedDrink);
    const isRepeated = getTogetherDrinks.some(
      (item) => item.id === selectedDrink.id
    );

    if (isRepeated) {
      toast.error(
        `${selectedDrink.name} is already on the get-togheter event list! Add it to another event or select another drink.`
      );
    } else {
      toast.success(
        `${selectedDrink.name} has been added to the get-together event list.`
      );
    }
  };

  const handleClickWedding = (selectedDrink) => {
    addToWeddingDrinks(selectedDrink);
    const isRepeated = weddingDrinks.some(
      (item) => item.id === selectedDrink.id
    );

    if (isRepeated) {
      toast.error(
        `${selectedDrink.name} is already on the wedding party list! Add it to another event or select another drink.`
      );
    } else {
      toast.success(
        `${selectedDrink.name} has been added to the wedding party list.`
      );
    }
  };

  const classes = useStyle();
  return (
    <Container>
      <Content>
        <div>
          <IconButton
            className={classes.closeButton}
            onClick={() => setIsShow(false)}
          >
            <CloseIcon />
          </IconButton>
        </div>
        <h2>{selectedDrink.name}</h2>
        <p>Select which event you want to add the drink to.</p>
        <Buttons>
          <button onClick={() => handleClickGraduation(selectedDrink)}>
            Graduation Party
          </button>
          <button onClick={() => handleClickGetTogether(selectedDrink)}>
            Get-together
          </button>
          <button onClick={() => handleClickWedding(selectedDrink)}>
            Wedding party
          </button>
        </Buttons>
        {/* {alreadyAddedGraduation && (
          <p className="message">
            {selectedDrink.name} is already on the
            <strong> graduation party</strong> list! Add it to another event or
            select another drink.
          </p>
        )}
        {alreadyAddedGetTogether && (
          <p className="message">
            {selectedDrink.name} is already on the
            <strong> get-together event</strong> list! Add it to another event
            or select another drink.
          </p>
        )}
        {alreadyAddedWedding && (
          <p className="message">
            {selectedDrink.name} is already on the
            <strong> wedding party</strong> list! Add it to another event or
            select another drink.
          </p>
        )} */}
      </Content>
    </Container>
  );
};

export default ChoiceOfEvent;

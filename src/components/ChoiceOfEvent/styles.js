import styled from "styled-components";

export const Container = styled.div`
  position: fixed;
  top: 0;
  z-index: 1;
  background-color: rgba(66, 66, 66, 0.616);
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Content = styled.div`
  background-color: var(--white);
  width: 100%;
  border-radius: 4px;

  div {
    display: flex;
    justify-content: flex-end;
  }

  h2 {
    color: var(--gray);
    text-align: center;
    font-size: 1.8rem;
  }

  p {
    color: var(--ligthgray);
    display: block;
    font-size: 1.2rem;
    text-align: center;
    margin: 20px 12px;
  }

  @media (min-width: 768px) {
    width: 50%;
  }

  .message {
    color: red;
    font-size: 0.9rem;
  }
`;

export const Buttons = styled.section`
  display: flex;
  justify-content: space-around;
  padding: 15px 0;
  button {
    cursor: pointer;
    padding: 5px;
    font-size: 0.9rem;
    font-weight: bold;
    background-color: var(--brown);
    color: var(--white);
    border-radius: 4px;

    &:hover {
      background-color: var(--black);
    }
  }
`;

import { Container } from "./styles";

const CatalogueCard = ({ drink, handleClick }) => {
  return (
    <Container onClick={() => handleClick(drink)}>
      <figure>
        <img src={drink.image_url} alt={drink.name} />
      </figure>
      <h2>{drink.name}</h2>
      <strong>{drink.description}</strong>
      <div>
        <span>First brewed: {drink.first_brewed}</span>
      </div>
      <div>
        <span>
          {drink.volume.value} {drink.volume.unit}
        </span>
      </div>
    </Container>
  );
};

export default CatalogueCard;

import { useGraduationDrinks } from "../../providers/graduationDrinks";
import { useGetTogetherDrinks } from "../../providers/getTogetherDrinks";
import { useWeddingDrinks } from "../../providers/weddingDrinks";
import { Container, ButtonBox } from "./styles";

const EventListCard = ({ name, drink, index }) => {
  const { removeFromGraduationDrinks } = useGraduationDrinks();
  const { removeFromGetTogetherDrinks } = useGetTogetherDrinks();
  const { removeFromWeddingDrinks } = useWeddingDrinks();

  const handleClick = (name, index) => {
    if (name === "graduation") {
      removeFromGraduationDrinks(index);
    } else if (name === "getTogether") {
      removeFromGetTogetherDrinks(index);
    } else {
      removeFromWeddingDrinks(index);
    }
  };

  return (
    <Container>
      <figure>
        <img src={drink.image_url} alt={drink.name} />
      </figure>
      <div>
        <h3>{drink.name}</h3>
        <p>First brewed: {drink.first_brewed}</p>
        <p>
          {drink.volume.value} {drink.volume.unit}
        </p>
      </div>
      <ButtonBox>
        <button onClick={() => handleClick(name, index)}>Remove</button>
      </ButtonBox>
    </Container>
  );
};

export default EventListCard;

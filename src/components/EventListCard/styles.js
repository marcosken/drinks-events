import styled from "styled-components";

export const Container = styled.li`
  display: flex;
  align-items: center;
  min-height: 110px;
  border: 5px ridge var(--brown);
  margin: 5px;
  padding: 10px;
  background-color: var(--black);
  color: var(--white);

  figure {
    display: none;
  }

  div {
    flex-grow: 1;
    padding: 10px 10px 10px 20px;
    h3,
    p {
      margin: 5px 0;
    }
  }

  @media (min-width: 768px) {
    figure {
      width: 80px;
      min-height: 110px;
      padding: 5px;
      border: 2px solid var(--brown);
      display: flex;
      align-items: center;
      background-color: var(--white);
    }

    img {
      display: block;
      width: 25px;
      margin: 0 auto;
    }
  }
`;

export const ButtonBox = styled.div`
  padding: 0;
  text-align: end;
  button {
    cursor: pointer;
    font-size: 1rem;
    font-weight: bold;
    color: var(--white);
    border-radius: 4px;
    padding: 6px;
    background-color: var(--brown);
  }
`;

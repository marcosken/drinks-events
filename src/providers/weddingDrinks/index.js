import { createContext, useState, useContext, useEffect } from "react";

const WeddingDrinksContext = createContext();

export const WeddingDrinksProvider = ({ children }) => {
  const [weddingDrinks, setWeddingDrinks] = useState([]);

  useEffect(() => {
    setWeddingDrinks(
      JSON.parse(localStorage.getItem("@drinksEvents:weddingDrinks")) || []
    );
  }, []);

  const addToWeddingDrinks = (drink) => {
    const weddingDrinksList =
      JSON.parse(localStorage.getItem("@drinksEvents:weddingDrinks")) || [];

    const isRepeated = weddingDrinks.some((item) => item.id === drink.id);

    if (!isRepeated) {
      weddingDrinksList.push(drink);
      localStorage.setItem(
        "@drinksEvents:weddingDrinks",
        JSON.stringify(weddingDrinksList)
      );

      setWeddingDrinks([...weddingDrinks, drink]);
    }
  };

  const removeFromWeddingDrinks = (index) => {
    const weddingDrinksList = weddingDrinks.filter((_, idx) => idx !== index);

    localStorage.setItem(
      "@drinksEvents:weddingDrinks",
      JSON.stringify(weddingDrinksList)
    );

    setWeddingDrinks(weddingDrinksList);
  };

  return (
    <WeddingDrinksContext.Provider
      value={{ weddingDrinks, addToWeddingDrinks, removeFromWeddingDrinks }}
    >
      {children}
    </WeddingDrinksContext.Provider>
  );
};

export const useWeddingDrinks = () => useContext(WeddingDrinksContext);

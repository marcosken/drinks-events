import { CatalogueProvider } from "./catalogue";
import { GetTogetherDrinksProvider } from "./getTogetherDrinks";
import { GraduationDrinksProvider } from "./graduationDrinks";
import { WeddingDrinksProvider } from "./weddingDrinks";

const Providers = ({ children }) => {
  return (
    <CatalogueProvider>
      <GraduationDrinksProvider>
        <GetTogetherDrinksProvider>
          <WeddingDrinksProvider>{children}</WeddingDrinksProvider>
        </GetTogetherDrinksProvider>
      </GraduationDrinksProvider>
    </CatalogueProvider>
  );
};

export default Providers;

import { createContext, useState, useContext, useEffect } from "react";

const GraduationDrinksContext = createContext();

export const GraduationDrinksProvider = ({ children }) => {
  const [graduationDrinks, setGraduationDrinks] = useState([]);

  useEffect(() => {
    setGraduationDrinks(
      JSON.parse(localStorage.getItem("@drinksEvents:graduationDrinks")) || []
    );
  }, []);

  const addToGraduationDrinks = (drink) => {
    const graduationDrinksList =
      JSON.parse(localStorage.getItem("@drinksEvents:graduationDrinks")) || [];

    const isRepeated = graduationDrinks.some((item) => item.id === drink.id);

    if (!isRepeated) {
      graduationDrinksList.push(drink);
      localStorage.setItem(
        "@drinksEvents:graduationDrinks",
        JSON.stringify(graduationDrinksList)
      );

      setGraduationDrinks([...graduationDrinks, drink]);
    }
  };

  const removeFromGraduationDrinks = (index) => {
    const graduationDrinksList = graduationDrinks.filter(
      (_, idx) => idx !== index
    );

    localStorage.setItem(
      "@drinksEvents:graduationDrinks",
      JSON.stringify(graduationDrinksList)
    );

    setGraduationDrinks(graduationDrinksList);
  };

  return (
    <GraduationDrinksContext.Provider
      value={{
        graduationDrinks,
        addToGraduationDrinks,
        removeFromGraduationDrinks,
      }}
    >
      {children}
    </GraduationDrinksContext.Provider>
  );
};

export const useGraduationDrinks = () => useContext(GraduationDrinksContext);

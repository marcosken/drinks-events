# Drink Events 🍻

## Índice

- **[Sobre o Projeto](#%EF%B8%8F-sobre-o-projeto)**
- **[Demonstração](#-demonstração)**
- **[Funcionalidades](#-funcionalidades)**
- **[Tecnologias](#%EF%B8%8F-tecnologias)**
- **[Autor](#-autor)**


## 🖥️ Sobre o Projeto

> Projeto realizado como parte integrante das atividades do curso da [Kenzie Academy Brasil](https://kenzie.com.br/)

Este projeto consiste em uma plataforma para auxiliar a administração de eventos.

Seu objetivo é facilitar a organização das bebidas de diferentes eventos, de maneira que o usuário pode selecionar as bebidas desejadas, bem como, verificar a lista de bebidas de cada evento.

## 🚀 Demonstração

**Deploy da aplicação**: [![Vercel](https://img.shields.io/badge/vercel-%23000000.svg?style=flat-square&logo=vercel&logoColor=white)](https://drinks-events-alpha.vercel.app/)

<p align="center">
    <img src="./src/assets/home.png" width=700/>
</p>

## 💡 Funcionalidades

- [x] Tela de listagem de bebidas
  - [x] Exibição de todas as opções de bebidas disponíveis
  - [x] Adição de bebida na listagem de um evento
- [x] Tela de cada evento
  - [x] Exibição da lista de bebidas selecionadas para cada evento
  - [x] Remoção de bebidas da lista

## 🛠️ Tecnologias

### Front-end

- [React JS](https://pt-br.reactjs.org/)
- [Styled Components](https://styled-components.com/)
- [Material UI](https://mui.com/pt/)
- [React Router DOM](https://mui.com/pt/)
- [Axios](https://axios-http.com/ptbr/)

### Back-end

- [Punk API](https://punkapi.com/documentation/v2)

## 👨‍💻 Autor

<img style="border-radius: 15%;" src="https://gitlab.com/uploads/-/system/user/avatar/8603970/avatar.png?width=400" width="70px;" alt=""/>

Marcos Kenji Kuribayashi

[![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-blue?style=flat-square&logo=Linkedin&logoColor=white)](https://www.linkedin.com/in/marcos-kuribayashi/) [![Gmail Badge](https://img.shields.io/badge/-marcosken13@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white)](mailto:marcosken13@gmail.com)


---

Desenvolvido por Marcos Kenji Kuribayashi 😉 
